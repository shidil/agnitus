<!-- Event -->  
  <div id="event" class="event"> 
    <div class="container"> 
    
      <div class="row space90"></div>
      
      <div class="row">   
        <div class="span12">   
          <!--<h2>Christmas Event</h2>-->
          <h4 class="theme-color">Read Detail Information</h4>
        </div>
      </div>  
      <div class="row space40"></div>  
         
      <div class="row">   
        <div class="span6 e-left">   

        </div>    
        <div class="span6 e-right">   
          <h4>Date & Time</h4>
        </div>
        <div class="span12 e-icon"> 
          <i class="icon-time"></i>
        </div>                    
      </div>         
      
      <div class="row info-row">   
        <div class="span12 content-right">
          <div class="row">  
            <div class="span5 offset6">
              <p>   
                14.2.2014<br />
                Start 9:00 am  
              </p>
            </div>
          </div>
        </div>
      </div>  

      <div class="row">   
        <div class="span6 e-left">   
          <h4>Event Location</h4>
        </div>    
        <div class="span6 e-right">   

        </div>
        <div class="span12 e-icon"> 
          <i class="icon-location-arrow"></i>
        </div>                    
      </div>   
      
      <div class="row info-row">   
        <div class="span12 content-left">
          <div class="row">  
            <div class="span5 offset1">
              <p>
                College of Engineering Thalassery<br />
               Kannur   
              </p>
            </div>
          </div>
        </div>
      </div>   

      <div class="row">   
        <div class="span6 e-left">   

        </div>    
        <div class="span6 e-right">   
          <h4>Info</h4>
        </div>
        <div class="span12 e-icon"> 
          <i class="icon-comment-alt"></i>
        </div>                    
      </div>      

      <div class="row info-row">   
        <div class="span12 content-right">
          <div class="row">  
            <div class="span5 offset6">
              <p>   
					Science may set limit to knowledge, but it does not set limits to imagination.
					Come February, Tech will be in Thalassery air.. Breath it in!. you think you still need an invitation?!.
              </p>
            </div>
          </div>
        </div>
      </div>   

      <div class="row">   
        <div class="span12">
          <div class="circle">
            <i class="icon-bell"></i>
          </div>
        </div>
      </div>      
      
      <div class="row space100"></div>
      
    </div>
  </div>
  <!-- Event End--> 
  