<!-- Footer -->  
  <footer id="contact" class="footer">
    <div class="container">  

      <div class="row space90"></div> 
      
      <div class="row">  
        <div class="span12">
          <h2 class="dark">GET IN TOUCH</h2>
          <h4>Contact Information</h4>
        </div>
      </div> 
    
      <div class="row space50"></div> 
    
      <div class="row">
        <div class="span6">
          <h6>Contact Form</h6>
          <!-- Contact Form -->  
          <form class="form-comment" method="post" action="#" />
            <input type="text" class="span6" name="mail" placeholder="E-mail *" value="" />
            <input type="text" class="span6" name="name" placeholder="Name" value="" />
            <input type="text" class="span6" name="url" placeholder="URL" value="" />
            <textarea class="span6" name="message" placeholder="Message *"></textarea>
            <div class="row space10"></div>
            <button class="btn right">Send Message</button>
          </form>
          <!-- Contact Form End -->   
        </div>
        <div class="span6">
          <div class="row">
            <div class="span3">
              <h6>Address</h6>
              <p>
                College of Engineering Thalassery<br />
                Kundoormala PO Eranholi<br />
                Thalassery <br/>
                Kannur Dist. Kerala
              </p>
            </div>
            <div class="span3">
              <h6>Contact Us</h6>
              <p>
                <i class="icon-phone"></i>Phone<br />
                <i class="icon-envelope-alt"></i><a href="agnitus2k14@gmail.com">info@example.com</a><br />
                <i class="icon-home"></i><a href="http://www.agnitus.org" rel="external">www.agnitus.org</a>
              </p>
            </div>
          </div>  
          
          <div class="space40"></div> 
          
          <h6>Location</h6>
 <iframe width="425" height="280" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps/ms?msa=0&amp;msid=216760218607614294401.0004d0d986ea7467bd4c5&amp;ie=UTF8&amp;t=m&amp;ll=11.787375,75.530319&amp;spn=0.094103,0.145912&amp;z=12&amp;output=embed"></iframe>        </div>
      </div>
    
      <div class="row space80"></div> 
    
      <div class="row">
        <div class="span12 social-container">
          <div class="social-inner">
            <div class="share-text">
              <p>Like it? Share it!</p>
            </div>
            <div class="social">
              <a href="#"><i class="icon-facebook a-bounce"></i></a>
              <a href="#"><i class="icon-twitter a-bounce"></i></a>
              <a href="#"><i class="icon-google-plus a-bounce"></i></a>
            </div>
          </div>          
        </div>
      </div>
      
      <div class="row space80"></div> 
    
    </div>
  </footer>   
  <!-- Footer End -->  